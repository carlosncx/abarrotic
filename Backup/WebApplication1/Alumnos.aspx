﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Alumnos.aspx.cs" Inherits="WebApplication1.Alumnos" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Avances</title>
    <style type="text/css">
                   *
{ margin: 0;
  padding: 0;}

body
{ font-family: 'trebuchet ms', arial, sans-serif;
  padding: 0px;
  margin: 0px;
  font-size: .80em;
  background: #E9E4C7 url(imagenes/pattern.png);
  color: #555;}

p
{ margin: 0px;
  padding: 0px 0px 16px 0px;
  line-height: 1.7em;}

h1, h2, h3, h4, h5, h6 
{ color: #362C20;
  padding: 0 0 5px 0;}

h1, h4
{ font: normal 175% "century gothic", arial;
  margin: 0 0 15px 0;
  padding: 15px 20px 5px 0;
  color: #444;}

h2, h5
{ padding: 0 0 5px 0;
  font: normal 110% arial;
  text-transform: uppercase;
  letter-spacing: 0;}

h3, h6
{ color: #888;
  font: normal 95% arial;
  letter-spacing: 0;
  padding: 0 0 15px 0;}

h4
{ color: #362C20;}

h5, h6
{ color: #129991;}

img
{ border: 0px; 
  margin: 0px; 
  padding: 0px;}

a, a:hover
{ border-bottom: 1px dotted;
  color: #444;
  outline: none;
  text-decoration: none;}

a:hover
{ border-bottom: 1px solid;}

form
{ padding: 0; 
  margin: 0;}

.left
{ float: left;
  width: auto;
  margin-right: 10px;}

.right
{ float: right; 
  width: auto;
  margin-left: 10px;}

.center
{ display: block;
  text-align: center;
  margin: 20px auto;}

blockquote
{ margin: 20px 0; 
  padding: 10px 20px 0 20px;
  border: 1px solid #E5E5DB;
  background: #FFF;}

ul
{ margin: 2px 0px 18px 16px;
  padding: 0px;}

ul li
{ list-style-type: square;
  margin: 0px 0px 6px 0px; 
  padding: 0px;}

ol
{ margin: 8px 0px 0px 24px;
  padding: 0px;}

ol li
{ margin: 0px 0px 11px 0px; 
  padding: 0px;}

#header, #logo, #menubar, #panel, #site_content, #footer
{ margin-left: auto; 
  margin-right: auto;}

#header
{ height: 152px;
  background: url(imagenes/back.png) repeat-x;}

#logo
{ width: 870px;
  position: relative;
  height: 107px;}

#logo #logo_text 
{ position: absolute; 
  top: 15px;
  left: 0;}

#logo h1
{ font: normal 300% "century gothic", arial, sans-serif;
  border-bottom: 0;
  text-transform: none;
  letter-spacing: 0.1em;
  padding: 12px 0 0 26px;
  color: #FFF;
  margin: 0;}

#menubar
{ width: 870px;
  height: 45px;
  padding-left: 5px;} 

ul#menu
{ float: left;
  margin: 0;}

ul#menu li
{ float: left;
  margin: 10px 0 0 0;
  padding: 0 0 0 14px;
  list-style: none;}

ul#menu li a
{ letter-spacing: 0.1em;
  font-size: 105%;
  display: block; 
  float: left; 
  height: 20px;
  text-decoration: none; 
  padding: 8px 26px 5px 12px;
  text-align: center;
  color: #FFF;
  border: none;} 

ul#menu li.tab_selected a
{ height: 22px;
  padding: 8px 26px 5px 12px;}

ul#menu li.tab_selected
{ margin: 10px 0 0 0;
  background: url(imagenes/tabs.png) no-repeat 0 0px;}

ul#menu li.tab_selected a
{ background: url(imagenes/tabs.png) no-repeat 100% 0px;
  color: #A2AB3B;}

ul#menu li.tab_selected a:hover, ul#menu li a:hover
{ color: #A2AB3B;}

#panel
{ width: 792px;
  height: 154px;
  border: 15px solid #E9E4C7;} 

#site_content
{ width: 875px;
  overflow: hidden;
  background: #FFF url(imagenes/site_content.png) repeat-y;
  padding-top: 20px;
            height: 1603px;
        } 

#site_content_bottom
{ width: 875px;
  height: 5px;
  clear: both;
  background: url(imagenes/site_content_bottom.png) no-repeat;}

.sidebar
{ float: right;
  width: 178px;
  padding: 20px 25px 15px 15px;}

.sidebar ul
{ border-top: 1px solid #D6D9C9;
  width: 178px; 
  padding-top: 4px; 
  margin: 4px 0px 30px 0px;}

.sidebar li
{ list-style: none; 
  padding: 0px 0px 4px 0px; 
  border-bottom: 1px solid #D6D9C9;}

.sidebar li a, .sidebar li a:hover
{ text-decoration: none; 
  padding: 0px 0px 0px 18px;
  display: block;
  background: transparent url(imagenes/arrow.png) no-repeat left center;
  color: #555;
  border: none;} 

.sidebar li a.selected, .sidebar li a:hover, .sidebar li a.selected:hover
{ background: transparent url(imagenes/arrow_select.png) no-repeat left center;} 

#content
{ text-align: left;
  width: 597px;
  padding: 20px 10px 15px 26px;}

#footer
{ width: 730px;
  height: 0px;
  padding: 18px 20px 4px 20px;
  text-align: center; 
  color: #444;
        }

.alternate_colour{color: #A2AB3B;}

.form_settings
{ margin: 15px 0 0 0;}

.form_settings p
{ padding: 0 0 4px 0;}

.form_settings span
{ float: left; 
  width: 200px; 
  text-align: left;}
  
.form_settings input, .form_settings textarea
{ padding: 2px; 
  width: 299px; 
  font: 100% arial; 
  border: 1px solid #E5E5DB; 
  background: #FFF; 
  color: #47433F;}
  
.form_settings .submit
{ font: 100% arial; 
  border: 1px solid; 
  width: 99px; 
  margin: 0 0 0 206px; 
  height: 26px;
  padding: 2px 0 3px 0;
  cursor: pointer; 
  background: #3B3B3B; 
  color: #FFF;}

.form_settings textarea, .form_settings select
{ font: 100% arial; 
  width: 299px;}

.form_settings select
{ width: 304px;}

.form_settings .checkbox
{ margin: 4px 0; 
  padding: 0; 
  width: 14px;
  border: 0;
  background: none;}

.separator
{ width: 100%;
  height: 0;
  border-top: 1px solid #D9D5CF;
  border-bottom: 1px solid #FFF;
  margin: 0 0 20px 0;}
  
table
{ margin: 10px 0 30px 0;}

table tr th, table tr td
{ text-align: left;
  background: #3B3B3B;
  color: #FFF;
  padding: 7px 4px;}
  
table tr td
{ background: #CCCCCC;
  color: #47433F;
  border-top: 1px solid #FFF;}
              
        
    </style>
</head>
<body>
    <form id="form1" runat="server" >
    <body>
  <div id="main">
    <div id="links"></div>
    <div id="header">
      <div id="logo">
        <div id="logo_text">
          <h1>Abarro<span class="alternate_colour">tic</span></h1>
        </div>
      </div>
      <div id="menubar">
        <ul id="menu">
          <!-- put class="tab_selected" in the li tag for the selected page - to highlight which page you're on -->
          <li><a href="Default.aspx">Inicio</a></li>
          <li><a href="">Catalogo</a></li>
          <li><a href="">Registrarse</a></li>
          <li><a href="Login.aspx">Login</a></li>
          <li><a href="Contactanos.aspx">Acerca de</a></li>
           <li class="tab_selected"><a href="Alumnos.aspx">Alumnos</a></li>
      </div>
    </div>
    <div id="site_content">
      <div id="panel"><img src="imagenes/marcas banner.jpg" alt="tree tops" /></div>


      <h1>Sprint #2</h1>

        <h1 class="style2">Altas de Proveedores y Diseño de la Pagina By Aaron Gracia</h1><a href="Proveedores.aspx">Sprint #2 ,Semana #1,Altas de Proveedores,27/ Septiembre/2014 y 28/ Septiembre/2014 </a>

        <h1 class="style2">Bajas de Proveedores By Juan Reyes</h1><a href="Proveedores.aspx">Sprint #2,Semana #2,Bajas de Proveedores, 03/ Octubre /2014</a>

         <h1 class="style2">Consultas Y Modificacion de Proveedores By Jairo Valencia</h1><a href="Proveedores.aspx">Sprint #2,Semana #2,Consulta Y Modificacion de Proveedores,02/Octubre/2014 y 05/ Octubre /2014 </a>
       
        <br />
        <br />
       
       <br>


       <h1>Sprint #3</h1>

        <h1 class="style2">Altas de Clientes y Diseño del CRUD By Aaron Gracia</h1><a href="Clientes.aspx">Sprint #3 ,Semana #1,Altas de Clientes,13/octubre/2014 y 16/octubre/2014</a>

        <h1 class="style2">Bajas de Clientes By Juan Reyes</h1><a href="Clientes.aspx">Sprint #3,Semana #2,Bajas de Clientes, 20/octubre/2014 </a>

         <h1 class="style2">Consultas Y Modificacion de Clientes By Jairo Valencia</h1><a href="Clientes.aspx">Sprint #3,Semana #2,Consulta Y Modificacion de Clientes,18/octubre/2014 y 21/octubre/2014 </a>
       

       <br>
       <br>
       <br>
       <h1>Sprint #4</h1>
        <h1 class="style2">Diseño de formato para crud productos By Juan Reyes</h1><a href="Productos.aspx">Sprint #4 ,Semana #1,Diseño Crud de Productos"24 de octubre del 2014"</a>
        <h1 class="style2">Altas de Productos By Aaron Gracia</h1><a href="Productos.aspx">Sprint #4 ,Semana #1,Altas de Productos"27 de octubre"</a>
        <h1 class="style2">Pagina de inicio(Informacion e introduccion a la pagina) by Jairo Valencia</h1><a href="Default.aspx">Sprint #4 ,Semana #1, Pagina de inicio"29 de octubre del 2014" </a>
        <h1 class="style2">Bajas de Productos  By Jairo Valencia</h1><a href="Productos.aspx">Sprint #4,Semana #2,Bajas de Productos,"3 de noviembre del 2014"</a>
         <h1 class="style2">Consultas de Productos By Aaron Gracia</h1><a href="Productos.aspx">Sprint #4,Semana #2,Consulta ,"30 de octubre del 2014" </a>
          <h1 class="style2">Modificacion de Productos By Juan Reyes</h1><a href="Productos.aspx">Sprint #4 ,Semana #2,Modificacion de Productos"1 de Noviembre del 2014"</a>
           <br>
       <br>
       <br>
       <h1></h1>
       </div>
        <div id="footer">Copyright &copy; Abarrotic. All Rights Reserved.  </div>
    </form>
   
</body>
</html>


