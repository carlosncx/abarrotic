﻿using System;
using System.Collections.Generic;
using System.Web;

namespace WebApplication1
{
    public class CsProveedores
    {
        // Declarar Atributos

        private int id_prov;

        private string nombre_prov;
        private string calles_prov;
        private string num_dep;
        private string colonia_prov;
        private string ciudad_prov;
        private string telefono_prov;
        private string codigo_postal_prov;
       



        // Propiedades



        public int Id_prov
        {

            get { return this.id_prov; }

            set { this.id_prov = value; }

        }


        public string Nombre_prov
        {

            get { return this.nombre_prov; }

            set { this.nombre_prov = value; }

        }

        public string Calles_prov
        {

            get { return this.calles_prov; }

            set { this.calles_prov = value; }

        }
        public string Num_dep
        {

            get { return this.num_dep; }

            set { this.num_dep = value; }

        }
        public string Colonia_prov
        {

            get { return this.colonia_prov; }

            set { this.colonia_prov = value; }

        }


        public string Ciudad_prov
        {

            get { return this.ciudad_prov; }

            set { this.ciudad_prov = value; }

        }



        public string Telefono_prov
        {

            get { return this.telefono_prov; }

            set { this.telefono_prov = value; }

        }


        public string Codigo_postal_prov
        {

            get { return this.codigo_postal_prov; }

            set { this.codigo_postal_prov = value; }

        }


        public bool Guardar()
        {

            string query = "insert into proveedores (nombre_prov,calles_prov,num_dep,colonia_prov,ciudad_prov,telefono_prov,codigo_postal_prov) values ";
 
            query += "('" + this.nombre_prov + "', '";
            query += this.calles_prov + "', '";
            query += this.num_dep + "', '";
            query += this.colonia_prov + "', '";
            query += this.ciudad_prov + "', '";
            query += this.telefono_prov + "', '";
            query += this.codigo_postal_prov + "') ";



            try
            {



                Clases.CsMysql BD = new Clases.CsMysql();

                BD.AbrirConexion();

                BD.EjecutarSql(query);

                BD.CerrarConexion();

                return true;

            }


            catch (Exception ex)
            {

                throw new Exception("Error al intentar guardar el Registro" + ex.Message);

            }

        }



        public bool Modificar()
        {

            string query = "update proveedores set ";       
            query += "nombre_prov= '" + this.nombre_prov + "', ";
            query += "calles_prov = '" + this.calles_prov + "',";
            query += "num_dep = '" + this.num_dep + "',";
            query += "colonia_prov = '" + this.colonia_prov + "',";
            query += "ciudad_prov = '" + this.ciudad_prov + "',";
            query += "telefono_prov = '" + this.telefono_prov + "',";
            query += "codigo_postal_prov = '" + this.codigo_postal_prov + "'";
            query += " where id_prov = " + this.id_prov;

            try
            {

                Clases.CsMysql BD = new Clases.CsMysql();

                BD.AbrirConexion();

                BD.EjecutarSql(query);

                BD.CerrarConexion();

                return true;

            }

            catch (Exception ex)
            {

                throw new Exception("Error al intentar Modificar el Registro" + ex.Message);

            }

        }

        public bool Eliminar(int casa)
        {

            string query = "delete from proveedores where id_prov = " + casa;

            try
            {

                Clases.CsMysql BD = new Clases.CsMysql();

                BD.AbrirConexion();

                BD.EjecutarSql(query);

                BD.CerrarConexion();

                return true;

            }

            catch (Exception ex)
            {

                throw new Exception("Error al intentar Eliminar Registro" + ex.Message);

            }
        }



        public bool Buscar(int proveedor)
        {

            string query = "select * from proveedores where id_prov = " + proveedor;

            try
            {



                Clases.CsMysql BD = new Clases.CsMysql();

                BD.AbrirConexion();

                BD.EjecutarConsulta(query);

                if (BD.ResultadoConsulta.Read())
                {
                    this.id_prov = Convert.ToInt32(BD.ResultadoConsulta["id_prov"]);
                    this.nombre_prov = BD.ResultadoConsulta["nombre_prov"].ToString();
                    this.calles_prov = BD.ResultadoConsulta["calles_prov"].ToString();
                    this.num_dep = BD.ResultadoConsulta["num_prov"].ToString();
                    this.colonia_prov = BD.ResultadoConsulta["colonia_prov"].ToString();
                    this.ciudad_prov = BD.ResultadoConsulta["ciudad_prov"].ToString();
                    this.telefono_prov = BD.ResultadoConsulta["telefono_prov"].ToString();
                    this.codigo_postal_prov = BD.ResultadoConsulta["codigo_postal_prov"].ToString();



                }

                BD.CerrarConexion();

                return true;

            }



            catch (Exception ex)
            {

                throw new Exception("Error al intentar Buscar el Registro" + ex.Message);



            }


        }
    }
}