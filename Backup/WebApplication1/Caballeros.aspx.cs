﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using Microsoft.Office.Interop.Word;

namespace WebApplication1
{
    public partial class Caballeros : System.Web.UI.Page
    {
        //int casa;
        //Clases.Ventasproductos cli = new Clases.CsClientes();
        Clases.csHerramientas herra = new Clases.csHerramientas();
        public string _Mensaje;
        protected void Page_Load(object sender, EventArgs e)
        {
            Lbl_Fecha.Text = DateTime.Now.ToString("yyyy/MM/dd");
            if (Session["sNomUsuario"] != null)
            {
                Lbl_Usuario.Text = Session["sNomUsuario"].ToString();
            }

                    
        }


        protected void ImageButton8_Click(object sender, ImageClickEventArgs e)
        {
            if (MessageBox.Show("" + Lbl_Producto.Text + " " + Lbl_Precio.Text + "$  ¿Quiere Agregar Este Producto A Su Compra?   " + "", "", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {

                Clases.CsVentaproducto Venta = new Clases.CsVentaproducto();


                try
                {

                    Venta.producto = Lbl_Producto.Text;
                    Venta.precio = Lbl_Precio.Text;
                    Venta.usuario = Lbl_Usuario.Text;
                    Venta.fecha_venta = Lbl_Fecha.Text;

                    Venta.Guardar();
                    MessageBox.Show("Ingresado Correctamente");


                    Response.Redirect("Caballeros.aspx");
                }
                catch (Exception ex)
                {

                    Response.Write(ex.Message);
                }
            }
        }
       

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Lacteos.aspx");
        }

        protected void ImageButton9_Click(object sender, ImageClickEventArgs e)
        {
            if (MessageBox.Show("" + Lbl_Producto1.Text + " " + Lbl_Precio0.Text + "$  ¿Quiere Agregar Este Producto A Su Compra?   " + "", "", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {

                try
                {
                    Clases.CsVentaproducto Venta = new Clases.CsVentaproducto();


                    Venta.producto = Lbl_Producto1.Text;
                    Venta.precio = Lbl_Precio0.Text;
                    Venta.usuario = Lbl_Usuario.Text;
                    Venta.fecha_venta = Lbl_Fecha.Text;

                    Venta.Guardar();
                    MessageBox.Show("Ingresado Correctamente");


                    Response.Redirect("Caballeros.aspx");
                }
                catch (Exception ex)
                {

                    Response.Write(ex.Message);

                }
            }
        }

        protected void ImageButton10_Click(object sender, ImageClickEventArgs e)
        {
            if (MessageBox.Show("" + Lbl_Producto2.Text + " " + Lbl_Precio1.Text + "$  ¿Quiere Agregar Este Producto A Su Compra?   " + "", "", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {

                try
                {
                    Clases.CsVentaproducto Venta = new Clases.CsVentaproducto();


                    Venta.producto = Lbl_Producto2.Text;
                    Venta.precio = Lbl_Precio1.Text;
                    Venta.usuario = Lbl_Usuario.Text;
                    Venta.fecha_venta = Lbl_Fecha.Text;

                    Venta.Guardar();
                    MessageBox.Show("Ingresado Correctamente");


                    Response.Redirect("Caballeros.aspx");
                }
                catch (Exception ex)
                {

                    Response.Write(ex.Message);

                }
            }
        }

        protected void ImageButton11_Click(object sender, ImageClickEventArgs e)
        {
            if (MessageBox.Show("" + Lbl_Producto3.Text + " " + Lbl_Precio2.Text + "$  ¿Quiere Agregar Este Producto A Su Compra?   " + "", "", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {

                try
                {
                    Clases.CsVentaproducto Venta = new Clases.CsVentaproducto();


                    Venta.producto = Lbl_Producto3.Text;
                    Venta.precio = Lbl_Precio2.Text;
                    Venta.usuario = Lbl_Usuario.Text;
                    Venta.fecha_venta = Lbl_Fecha.Text;

                    Venta.Guardar();
                    MessageBox.Show("Ingresado Correctamente");


                    Response.Redirect("Caballeros.aspx");
                }
                catch (Exception ex)
                {

                    Response.Write(ex.Message);

                }
            }
        }

        protected void ImageButton12_Click(object sender, ImageClickEventArgs e)
        {
            if (MessageBox.Show("" + Lbl_Producto4.Text + " " + Lbl_Precio3.Text + "$  ¿Quiere Agregar Este Producto A Su Compra?   " + "", "", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {

                try
                {
                    Clases.CsVentaproducto Venta = new Clases.CsVentaproducto();


                    Venta.producto = Lbl_Producto4.Text;
                    Venta.precio = Lbl_Precio3.Text;
                    Venta.usuario = Lbl_Usuario.Text;
                    Venta.fecha_venta = Lbl_Fecha.Text;

                    Venta.Guardar();
                    MessageBox.Show("Ingresado Correctamente");


                    Response.Redirect("Caballeros.aspx");
                }
                catch (Exception ex)
                {

                    Response.Write(ex.Message);

                }
            }
        }

        protected void ImageButton13_Click(object sender, ImageClickEventArgs e)
        {
            if (MessageBox.Show("" + Lbl_Producto5.Text + " " + Lbl_Precio4.Text + "$  ¿Quiere Agregar Este Producto A Su Compra?   " + "", "", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {

                try
                {
                    Clases.CsVentaproducto Venta = new Clases.CsVentaproducto();


                    Venta.producto = Lbl_Producto5.Text;
                    Venta.precio = Lbl_Precio4.Text;
                    Venta.usuario = Lbl_Usuario.Text;
                    Venta.fecha_venta = Lbl_Fecha.Text;

                    Venta.Guardar();
                    MessageBox.Show("Ingresado Correctamente");


                    Response.Redirect("Caballeros.aspx");
                }
                catch (Exception ex)
                {

                    Response.Write(ex.Message);

                }
            }
        }

        protected void ImageButton14_Click(object sender, ImageClickEventArgs e)
        {
            if (MessageBox.Show("" + Lbl_Producto6.Text + " " + Lbl_Precio5.Text + "$  ¿Quiere Agregar Este Producto A Su Compra?   " + "", "", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {

                try
                {
                    Clases.CsVentaproducto Venta = new Clases.CsVentaproducto();


                    Venta.producto = Lbl_Producto6.Text;
                    Venta.precio = Lbl_Precio5.Text;
                    Venta.usuario = Lbl_Usuario.Text;
                    Venta.fecha_venta = Lbl_Fecha.Text;

                    Venta.Guardar();
                    MessageBox.Show("Ingresado Correctamente");


                    Response.Redirect("Caballeros.aspx");
                }
                catch (Exception ex)
                {

                    Response.Write(ex.Message);

                }
            }
        }

        protected void ImageButton15_Click(object sender, ImageClickEventArgs e)
        {
            if (MessageBox.Show("" + Lbl_Producto7.Text + " " + Lbl_Precio6.Text + "$  ¿Quiere Agregar Este Producto A Su Compra?   " + "", "", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {

                try
                {
                    Clases.CsVentaproducto Venta = new Clases.CsVentaproducto();


                    Venta.producto = Lbl_Producto7.Text;
                    Venta.precio = Lbl_Precio6.Text;
                    Venta.usuario = Lbl_Usuario.Text;
                    Venta.fecha_venta = Lbl_Fecha.Text;

                    Venta.Guardar();
                    MessageBox.Show("Ingresado Correctamente");


                    Response.Redirect("Caballeros.aspx");
                }
                catch (Exception ex)
                {

                    Response.Write(ex.Message);

                }
            }
        }

        protected void ImageButton16_Click(object sender, ImageClickEventArgs e)
        {
            if (MessageBox.Show("" + Lbl_Producto8.Text + " " + Lbl_Precio7.Text + " $  ¿Quiere Agregar Este Producto A Su Compra?   " + "", "", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {

                try
                {
                    Clases.CsVentaproducto Venta = new Clases.CsVentaproducto();


                    Venta.producto = Lbl_Producto8.Text;
                    Venta.precio = Lbl_Precio7.Text;
                    Venta.usuario = Lbl_Usuario.Text;
                    Venta.fecha_venta = Lbl_Fecha.Text;

                    Venta.Guardar();
                    MessageBox.Show("Ingresado Correctamente");


                    Response.Redirect("Caballeros.aspx");
                }
                catch (Exception ex)
                {

                    Response.Write(ex.Message);

                }
            }
        }

        protected void ImageButton17_Click(object sender, ImageClickEventArgs e)
        {
            if (MessageBox.Show("" + Lbl_Producto9.Text + " " + Lbl_Precio8.Text + "$  ¿Quiere Agregar Este Producto A Su Compra?   " + "", "", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {

                try
                {
                    Clases.CsVentaproducto Venta = new Clases.CsVentaproducto();


                    Venta.producto = Lbl_Producto9.Text;
                    Venta.precio = Lbl_Precio8.Text;
                    Venta.usuario = Lbl_Usuario.Text;
                    Venta.fecha_venta = Lbl_Fecha.Text;

                    Venta.Guardar();
                    MessageBox.Show("Ingresado Correctamente");


                    Response.Redirect("Caballeros.aspx");
                }
                catch (Exception ex)
                {

                    Response.Write(ex.Message);

                }
            }
        }

        protected void ImageButton18_Click(object sender, ImageClickEventArgs e)
        {
            if (MessageBox.Show("" + Lbl_Producto10.Text + " " + Lbl_Precio9.Text + "$  ¿Quiere Agregar Este Producto A Su Compra?   " + "", "", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {

                try
                {
                    Clases.CsVentaproducto Venta = new Clases.CsVentaproducto();


                    Venta.producto = Lbl_Producto10.Text;
                    Venta.precio = Lbl_Precio9.Text;
                    Venta.usuario = Lbl_Usuario.Text;
                    Venta.fecha_venta = Lbl_Fecha.Text;

                    Venta.Guardar();
                    MessageBox.Show("Ingresado Correctamente");


                    Response.Redirect("Caballeros.aspx");
                }
                catch (Exception ex)
                {

                    Response.Write(ex.Message);

                }
            }
        }

        protected void ImageButton19_Click(object sender, ImageClickEventArgs e)
        {
            if (MessageBox.Show("" + Lbl_Producto11.Text + " " + LbL_Precio10.Text + "$  ¿Quiere Agregar Este Producto A Su Compra?   " + "", "", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {

                try
                {
                    Clases.CsVentaproducto Venta = new Clases.CsVentaproducto();


                    Venta.producto = Lbl_Producto11.Text;
                    Venta.precio = LbL_Precio10.Text;
                    Venta.usuario = Lbl_Usuario.Text;
                    Venta.fecha_venta = Lbl_Fecha.Text;

                    Venta.Guardar();
                    MessageBox.Show("Ingresado Correctamente");


                    Response.Redirect("Caballeros.aspx");
                }
                catch (Exception ex)
                {

                    Response.Write(ex.Message);

                }
            }
        }

        protected void ImageButton20_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Pagos_Y_Consultas.aspx");
        }

        protected void ImageButton21_Click(object sender, ImageClickEventArgs e)
        {
           
        }

        protected void ImageButton22_Click(object sender, ImageClickEventArgs e)
        {
            
        }

        protected void ImageButton23_Click(object sender, ImageClickEventArgs e)
        {
           
        }

        protected void ImageButton24_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Lacteos.aspx");
        }

        protected void ImageButton25_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Carnes.aspx");
        }

        protected void ImageButton26_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Damas.aspx");
        }

        protected void ImageButton27_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void ImageButton28_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Perfumes.aspx");
        }

        protected void ImageButton29_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void ImageButton30_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Zapateria.aspx");
        }

     

        protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Carnes.aspx");
        }

        protected void ImageButton3_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Damas.aspx");
        }

        protected void ImageButton4_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void ImageButton5_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Perfumes.aspx");
        }

        protected void ImageButton6_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void ImageButton7_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Zapateria.aspx");
        }
    }
}