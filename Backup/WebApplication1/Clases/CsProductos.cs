﻿using System;
using System.Collections.Generic;
using System.Web;

namespace WebApplication1.Clases
{
    public class CsProductos
    {
        private int id_prod;
        private string id_provf;
        private string fotografia;
        private string descripcion;
        private string precio;
        private string existencias;





        // Propiedades



        public int Id_prod
        {

            get { return this.id_prod; }

            set { this.id_prod = value; }

        }


        public string Fotografia
        {

            get { return this.fotografia; }

            set { this.fotografia = value; }

        }

        public string Descripcion
        {

            get { return this.descripcion; }

            set { this.descripcion = value; }

        }
        public string Precio
        {

            get { return this.precio; }

            set { this.precio = value; }

        }
        public string Existencias
        {

            get { return this.existencias; }

            set { this.existencias = value; }

        }

        public string Id_provf
        {

            get { return this.id_provf; }

            set { this.id_provf = value; }

        }





        public bool Guardar()
        {

            string query = "insert into productos (fotografia,descripcion,precio,existencias,id_provf) values ";

            query += "('" + this.fotografia + "', '";
            query += this.descripcion + "', '";
            query += this.precio + "', '";
            query += this.existencias + "', '";
            query += this.id_provf + "') ";




            try
            {



                Clases.CsMysql BD = new Clases.CsMysql();

                BD.AbrirConexion();

                BD.EjecutarSql(query);

                BD.CerrarConexion();

                return true;

            }


            catch (Exception ex)
            {

                throw new Exception("Error al intentar guardar el Producto" + ex.Message);

            }

        }



        public bool Modificar()
        {

            string query = "update productos set ";

            query += "fotografia= '" + this.fotografia + "', ";
            query += "descripcion = '" + this.descripcion + "',";
            query += "precio = '" + this.precio + "',";
            query += "existencias= '" + this.existencias + "',";
            query += "id_provf = '" + this.id_provf + "'";
            query += " where id_prod = " + this.id_prod;




            try
            {



                Clases.CsMysql BD = new Clases.CsMysql();

                BD.AbrirConexion();

                BD.EjecutarSql(query);

                BD.CerrarConexion();

                return true;

            }



            catch (Exception ex)
            {

                throw new Exception("Error al intentar Modificar el Producto" + ex.Message);



            }



        }



        public bool Eliminar(int prod)
        {

            string query = "delete from productos where id_prod = " + prod;



            try
            {



                Clases.CsMysql BD = new Clases.CsMysql();

                BD.AbrirConexion();

                BD.EjecutarSql(query);

                BD.CerrarConexion();

                return true;

            }



            catch (Exception ex)
            {

                throw new Exception("Error al intentar Eliminar Producto" + ex.Message);



            }



        }



        public bool Buscar(int producto)
        {

            string query = "select * from productos where id_prod = " + id_prod;



            try
            {



                Clases.CsMysql BD = new Clases.CsMysql();

                BD.AbrirConexion();

                BD.EjecutarConsulta(query);

                if (BD.ResultadoConsulta.Read())
                {
                    this.id_prod = Convert.ToInt32(BD.ResultadoConsulta["id_prod"]);
                    this.fotografia = BD.ResultadoConsulta["fotografia"].ToString();
                    this.descripcion = BD.ResultadoConsulta["descripcion"].ToString();
                    this.precio = BD.ResultadoConsulta["precio"].ToString();
                    this.existencias = BD.ResultadoConsulta["existencias"].ToString();
                    this.id_provf = BD.ResultadoConsulta["id_provf"].ToString();




                }

                BD.CerrarConexion();

                return true;

            }



            catch (Exception ex)
            {

                throw new Exception("Error al intentar Buscar el Producto" + ex.Message);



            }
        }
        public bool guardar_prod(string descripcion, string precio, string existencias, string direccion, string id_provf)
        {
            CsMysql BD = new CsMysql();
            string query = "insert into productos (fotografia,descripcion,precio,existencias,id_provf) values('" + direccion + "','" + descripcion + "','" + precio + "','" + existencias + "','" + id_provf + "');";
            try
            {
                BD.AbrirConexion();
                BD.EjecutarConsulta(query);
                BD.CerrarConexion();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al guardar el registro" + ex.Message);
            }
        }
        public bool modificar_admin(int id_prod, string descripcion, int precio, int existencias, string direccion, int id_provf)
        {
            CsMysql BD = new CsMysql();
            string query = "update productos set descripcion='" + descripcion + "', precio='" + precio + "', fotografia='" + fotografia + "', id_provf='" + id_provf + "' where id_prod=" + id_prod;
            try
            {
                BD.AbrirConexion();
                BD.EjecutarConsulta(query);
                BD.CerrarConexion();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al modificar el registro" + ex.Message);
            }
        }
        public bool modificar_admin2(string id, string usuario, string foto)
        {
            CsMysql BD = new CsMysql();
            string query = "update administradores set username='" + usuario + "', fotografia_adm='" + foto + "' where id_admn=" + id;
            try
            {
                BD.AbrirConexion();
                BD.EjecutarConsulta(query);
                BD.CerrarConexion();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al modificar el registro" + ex.Message);
            }
        }

        public bool modificar_admin3(string id, string usuario)
        {
            CsMysql BD = new CsMysql();
            string query = "update administradores set username='" + usuario + "' where id_admn=" + id;
            try
            {
                BD.AbrirConexion();
                BD.EjecutarConsulta(query);
                BD.CerrarConexion();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al modificar el registro" + ex.Message);
            }
        }

        public bool modificar_admin4(string id, string usuario, string clave)
        {
            CsMysql BD = new CsMysql();
            string query = "update administradores set username='" + usuario + "', password='" + clave + "' where id_admn=" + id;
            try
            {
                BD.AbrirConexion();
                BD.EjecutarConsulta(query);
                BD.CerrarConexion();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al modificar el registro" + ex.Message);
            }
        }
        public bool eliminar_registro(string tabla, string atributo, string valor)
        {
            CsMysql BD = new CsMysql();
            string query = "delete from " + tabla + " where " + atributo + " = '" + valor + "';";
            try
            {
                BD.AbrirConexion();
                BD.EjecutarConsulta(query);
                BD.CerrarConexion();
                return true;
            }
            catch (Exception ex)
            {

                throw new Exception("Error al eliminar el registro" + ex.Message);

            }
        }
    }
}