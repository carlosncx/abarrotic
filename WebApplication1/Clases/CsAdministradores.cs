﻿using System;
using System.Collections.Generic;
using System.Web;

namespace WebApplication1.Clases
{
    public class CsAdministradores
    {
        private int id_admn;
        private string username;
        private string password;
        private string direccion_fot;


        public int Id_admn
        {

            get { return this.id_admn; }

            set { this.id_admn = value; }

        }


        public string Username
        {

            get { return this.username; }

            set { this.username = value; }

        }

        public string Password
        {

            get { return this.password; }

            set { this.password = value; }

        }
        public string Direccion_fot
        {

            get { return this.direccion_fot; }

            set { this.direccion_fot = value; }

        }


        public bool Modificar()
        {

            string query = "update administradores set ";
            query += "username= '" + this.username + "', ";
            query += "password = '" + this.password + "',";
            query += "direccion_fot = '" + this.direccion_fot + "'";
            query += " where id_admn = " + this.id_admn;

            try
            {

                Clases.CsMysql BD = new Clases.CsMysql();

                BD.AbrirConexion();

                BD.EjecutarSql(query);

                BD.CerrarConexion();

                return true;

            }

            catch (Exception ex)
            {

                throw new Exception("Error al intentar Modificar el Registro" + ex.Message);

            }

        }
        public bool Eliminar(int casa)
        {

            string query = "delete from administradores where id_admn = " + casa;

            try
            {

                Clases.CsMysql BD = new Clases.CsMysql();

                BD.AbrirConexion();

                BD.EjecutarSql(query);

                BD.CerrarConexion();

                return true;

            }

            catch (Exception ex)
            {

                throw new Exception("Error al intentar Eliminar Registro" + ex.Message);

            }
        }
    }
}