﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class Proveedores : System.Web.UI.Page
    {
        int casa;
        CsProveedores prov = new CsProveedores();
        Clases.csHerramientas Tooli = new Clases.csHerramientas();
        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                CsProveedores prov = new CsProveedores();


                prov.Nombre_prov = txtnomb_prov.Text; 
                prov.Calles_prov = txtcallesprov.Text;
                prov.Num_dep = txtnum_dep.Text;
                prov.Colonia_prov = txtcoloniaprov.Text;
                prov.Ciudad_prov = txtciudadprov.Text;
                prov.Telefono_prov = txttelefonoprov.Text;
                prov.Codigo_postal_prov = txtcod_postalprov.Text;

                //cli.Foto = tooli.GenerarNombreArchivo(fufotocasa);



                if (prov.Guardar())
                {

                    //if (fufotocasa.HasFile)
                    //{

                    //    tooli.CargarImagen(fufotocasa, MapPath("img/" + cas.Foto));

                    //}

                }

                Response.Write("Grabado con Exito!");

                txtnomb_prov.Text = "";
                txtcallesprov.Text = "";
                txtnum_dep.Text = "";
                txtciudadprov.Text = "";
                txtcoloniaprov.Text = "";
                txttelefonoprov.Text = "";
                txtcod_postalprov.Text = "";
                Response.Redirect("Proveedores.aspx");

            }
            catch (Exception ex)
            {

                Response.Write(ex.Message);

            }
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtidprov.Text = gvProveedores.Rows[gvProveedores.SelectedIndex].Cells[0].Text;
            txtnomb_prov.Text = gvProveedores.Rows[gvProveedores.SelectedIndex].Cells[1].Text;
            txtcallesprov.Text = gvProveedores.Rows[gvProveedores.SelectedIndex].Cells[2].Text;
            txtnum_dep.Text = gvProveedores.Rows[gvProveedores.SelectedIndex].Cells[3].Text;
            txtcoloniaprov.Text = gvProveedores.Rows[gvProveedores.SelectedIndex].Cells[4].Text;
            txtciudadprov.Text = gvProveedores.Rows[gvProveedores.SelectedIndex].Cells[5].Text;
            txttelefonoprov.Text = gvProveedores.Rows[gvProveedores.SelectedIndex].Cells[6].Text;
            txtcod_postalprov.Text = gvProveedores.Rows[gvProveedores.SelectedIndex].Cells[7].Text;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Tooli.LlenarGridquery(gvProveedores, "select * from proveedores");
        }

        protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
        {
            //Leer las variables

             casa = Convert.ToInt32(txtidprov.Text);


             CsProveedores prov = new CsProveedores();
            try
            {



                prov.Eliminar(this.casa);

                Response.Write("Registro Eliminado!");

                txtnomb_prov.Text = "";

                txtcallesprov.Text = "";

                txtciudadprov.Text = "";

                txtcod_postalprov.Text = "";

                txtcoloniaprov.Text = "";

                txtnum_dep.Text = "";

                txttelefonoprov.Text = "";

                txtidprov.Text = "";
                Response.Redirect("Proveedores.aspx");

            }

            catch (Exception ex)
            {

                Response.Write(ex.Message);

            }
        }

        protected void ImageButton3_Click(object sender, ImageClickEventArgs e)
        {
            casa = Convert.ToInt32(txtidprov.Text);



            CsProveedores prov=new CsProveedores();

            try
            {

                prov.Id_prov = this.casa;
                prov.Nombre_prov = txtnomb_prov.Text;
                prov.Calles_prov = txtcallesprov.Text;
                prov.Num_dep =txtnum_dep.Text;
                prov.Colonia_prov = txtcoloniaprov.Text;
                prov.Ciudad_prov = txtciudadprov.Text;
                prov.Telefono_prov = txttelefonoprov.Text;
                prov.Codigo_postal_prov = txtcod_postalprov.Text;
                //Si selecciono un archivo


                prov.Modificar();


                Response.Write("Registro Modificado");


                Response.Redirect("Proveedores.aspx");

            }

            catch (Exception ex)
            {

                Response.Write(ex.Message);



            }
        }
    }
}