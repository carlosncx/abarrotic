﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblmensaje.Visible = Session["sNomUsuario"] == null;
        }

       
        public void ValidarUsuario(string UserName, string Password)
        {
            bool usuarioencontrado = false;
            lblmensaje.Visible = false;

            Clases.CsMysql BD = new Clases.CsMysql();

            try
            {
                BD.AbrirConexion();
                string query = "Select * from administradores where username = '" + UserName + "' and password = '" + Password + "'";
                BD.EjecutarConsulta(query);
                if (BD.ResultadoConsulta.HasRows && BD.ResultadoConsulta.Read())
                {
                    Session["sNomUsuario"] = BD.ResultadoConsulta["username"].ToString();
                    Session["sTipoUsuario"] = "Usuario";
                    Session["sIdUsuario"] = BD.ResultadoConsulta["password"].ToString();

                    lblmensaje.Visible = false;
                    usuarioencontrado = true;

                }
                BD.CerrarConexion();

                if (usuarioencontrado)
                {
                    Response.Redirect("Default.aspx");
                    return;
                }

                BD.AbrirConexion();
                query = "Select * from administradores where usuario_adm = '" + UserName + "' and contrasena_adm = '" + Password + "'";
                BD.EjecutarConsulta(query);
                if (BD.ResultadoConsulta.HasRows && BD.ResultadoConsulta.Read())
                {
                    Session["sNomUsuario"] = BD.ResultadoConsulta["username"].ToString();
                    Session["sTipoUsuario"] = "Administrador";
                    Session["sIdUsuario"] = BD.ResultadoConsulta["password"].ToString();

                    lblmensaje.Visible = false;
                    usuarioencontrado = true;
                }
                BD.CerrarConexion();

                if (usuarioencontrado)
                {
                    Response.Redirect("Administrador.aspx");
                    return;
                }

                if (!usuarioencontrado)
                {
                    lblmensaje.Visible = false;
                    lblmensaje.Text = "USUARIO Y/O CONTRASEÑA NO COINCIDEN";
                }
            }
            catch (Exception ex)
            {
                Response.Write("Error al intentar iniciar sesion.\n" + ex.Message);
            }
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            
        }

        protected void btniniciar_Click(object sender, EventArgs e)
        {
             ValidarUsuario(txtusuario.Text, txtpassword.Text);
        }

        protected void txtpassword_TextChanged(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Inicio.aspx");
        }

    }
}
