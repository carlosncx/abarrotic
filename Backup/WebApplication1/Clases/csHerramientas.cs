﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.WebControls;
using System.IO;
using MySql.Data.MySqlClient;
using System.Web.UI;

namespace WebApplication1.Clases
{
    public class csHerramientas
    {
        public bool guardar_prod(string descripcion, string precio, string existencias, string direccion, string id_provf)
        {
            CsMysql BD = new CsMysql();
            string query = "insert into productos (fotografia,descripcion,precio,existencias,id_provf) values('" + direccion + "','" + descripcion + "','" + precio + "','" + existencias + "','" + id_provf + "');";
            try
            {
                BD.AbrirConexion();
                BD.EjecutarConsulta(query);
                BD.CerrarConexion();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al guardar el registro" + ex.Message);
            }
        }
        public bool guardar_admin(string username, string password, string direccion)
        {
            CsMysql BD = new CsMysql();
            string query = "insert into administradores (username,password,direccion_fot) values('" + username + "','" + password + "','" + direccion + "');";
            try
            {
                BD.AbrirConexion();
                BD.EjecutarConsulta(query);
                BD.CerrarConexion();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al guardar el registro" + ex.Message);
            }
        }
        public bool modificar_admin(int id_admn, string fotografia, string username, string password)
        {
            CsMysql BD = new CsMysql();
            string query = "update administradores set username='" + username + "', password='" + password + "', direccion_fot='" + fotografia + "' where id_admn=" + id_admn;
            try
            {
                BD.AbrirConexion();
                BD.EjecutarConsulta(query);
                BD.CerrarConexion();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al modificar el registro" + ex.Message);
            }
        }
        public bool modificar_admin2(string id, string usuario, string foto)
        {
            CsMysql BD = new CsMysql();
            string query = "update administradores set username='" + usuario + "', fotografia_adm='" + foto + "' where id_admn=" + id;
            try
            {
                BD.AbrirConexion();
                BD.EjecutarConsulta(query);
                BD.CerrarConexion();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al modificar el registro" + ex.Message);
            }
        }

        public bool modificar_admin3(string id, string usuario)
        {
            CsMysql BD = new CsMysql();
            string query = "update administradores set username='" + usuario + "' where id_admn=" + id;
            try
            {
                BD.AbrirConexion();
                BD.EjecutarConsulta(query);
                BD.CerrarConexion();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al modificar el registro" + ex.Message);
            }
        }

        public bool modificar_admin4(string id, string usuario, string clave)
        {
            CsMysql BD = new CsMysql();
            string query = "update administradores set username='" + usuario + "', password='" + clave + "' where id_admn=" + id;
            try
            {
                BD.AbrirConexion();
                BD.EjecutarConsulta(query);
                BD.CerrarConexion();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al modificar el registro" + ex.Message);
            }
        }
        public void LlenarCombo(DropDownList combo, string Tabla, string campotexto, string campovalor, string condicion)
        {

            CsMysql BD = new CsMysql();

            string query = "select " + campotexto + "," + campovalor + " from " + Tabla;

            if (condicion != null && condicion.Length > 1)
            {

                query += " where " + condicion;

            }



            try
            {

                BD.AbrirConexion();

                BD.EjecutarConsulta(query);

                combo.Items.Clear();

                combo.Items.Add(new ListItem("--Seleccione Opcion--", "0"));

                while (BD.ResultadoConsulta.Read())
                {

                    combo.Items.Add(new ListItem(BD.ResultadoConsulta[campotexto].ToString(), BD.ResultadoConsulta[campovalor].ToString()));

                }

                combo.SelectedValue = "0";

                BD.CerrarConexion();

            }

            catch (Exception ex)
            {

                throw new Exception("Error al intentar llenar DropdownList:\n" + ex.Message);

            }



        }

        public void LlenarGrid(GridView grid, string Tabla, string atributo1, string atributo2, string atributo3, string condicion)
        {



            CsMysql BD = new CsMysql();

            string query = "select " + atributo1 + "," + atributo2 + "," + atributo3 + " from " + Tabla;

            if (condicion != null && condicion.Length > 1)
            {

                query += " where " + condicion;

            }



            BD.AbrirConexion();

            BD.EjecutarConsulta(query);

            grid.DataSource = BD.ResultadoConsulta;

            grid.DataBind();

            BD.CerrarConexion();



        }



        public void LlenarGrid2(GridView grid, string Tabla, string atributo1, string atributo2, string condicion)
        {



            CsMysql BD = new CsMysql();

            string query = "select " + atributo1 + "," + atributo2 + " from " + Tabla;

            if (condicion != null && condicion.Length > 1)
            {

                query += " where " + condicion;

            }



            BD.AbrirConexion();

            BD.EjecutarConsulta(query);

            grid.DataSource = BD.ResultadoConsulta;

            grid.DataBind();

            BD.CerrarConexion();



        }





        public void LlenarGridquery(GridView grid, string query)
        {

            CsMysql BD = new CsMysql();

            BD.AbrirConexion();

            BD.EjecutarConsulta(query);

            grid.DataSource = BD.ResultadoConsulta;

            grid.DataBind();

            BD.CerrarConexion();

        }





        public void LlenarImagen(Image image, string Tabla, string condicion, int posicion)
        {

            try
            {

                CsMysql BD = new CsMysql();

                string query = "select * from " + Tabla;

                if (condicion != null && condicion.Length > 1)
                {

                    query += " where " + condicion;

                }

                BD.AbrirConexion();

                BD.EjecutarConsulta(query);

                if (BD.ResultadoConsulta.Read())
                {

                    image.ImageUrl = BD.ResultadoConsulta.GetString(posicion);

                }

                BD.CerrarConexion();

            }



            catch (Exception ex)
            {

                throw new Exception("Error al intentar Buscar la Foto" + ex.Message);



            }

        }





        public void LlenarRepeater(Repeater repeaters, string Tabla, string condicion)
        {



            CsMysql BD = new CsMysql();

            string query = "select  *  from " + Tabla;

            if (condicion != null && condicion.Length > 1)
            {

                query += " where " + condicion;

            }



            try
            {

                BD.AbrirConexion();

                BD.EjecutarConsulta(query);

                repeaters.DataSource = BD.ResultadoConsulta;

                repeaters.DataBind();

                BD.CerrarConexion();



            }

            catch (Exception ex)
            {

                throw new Exception("Error al intentar buscar las Subcategorias" + ex.Message);

            }





        }



        public void LlenarGridview(GridView grid, string Tabla, string cadenatributos, string condicion)
        {



            CsMysql BD = new CsMysql();

            string query = "select " + cadenatributos + " from " + Tabla;

            if (condicion != null && condicion.Length > 1)
            {

                query += " where " + condicion;

            }



            BD.AbrirConexion();

            BD.EjecutarConsulta(query);



            grid.DataSource = BD.ResultadoConsulta;

            grid.DataBind();

            BD.CerrarConexion();



        }



        public bool CargarImagen(FileUpload fimagen, string NombreArchivo)
        {

            try
            {

                fimagen.SaveAs(NombreArchivo);

                return true;

            }

            catch (Exception ex)
            {

                throw new Exception("Error al cargar la imagen en el servidor:\n" + ex.Message);

            }

        }



        public bool RemplazarImagen(FileUpload fimagen, string NombreArchivo, string ArchivoViejo)
        {

            try
            {

                fimagen.SaveAs(NombreArchivo);

                if (File.Exists(ArchivoViejo))
                {

                    File.Delete(ArchivoViejo);

                }

                return true;

            }

            catch (Exception ex)
            {

                throw new Exception("Error al cargar la imagen en el servidor:\n" + ex.Message);

            }

        }



        public bool EliminarImagen(string NombreArchivo)
        {

            try
            {

                if (File.Exists(NombreArchivo))
                {

                    File.Delete(NombreArchivo);

                }

                return true;

            }

            catch (Exception ex)
            {

                throw new Exception("Error al cargar la imagen en el servidor:\n" + ex.Message);

            }

        }



        public string GenerarNombreArchivo(FileUpload archivo)
        {

            if (!archivo.HasFile)
            {

                return "";

            }



            string nuevonombre = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString();

            nuevonombre += "_" + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Millisecond.ToString();

            nuevonombre += archivo.FileName.Substring(archivo.FileName.IndexOf("."));



            return nuevonombre;

        }



        public void LlenarImagen2(Image image, string Tabla, string condicion, int posicion, string carpeta)
        {

            try
            {

                CsMysql BD = new CsMysql();

                string query = "select * from " + Tabla;

                if (condicion != null && condicion.Length > 1)
                {

                    query += " where " + condicion;

                }

                BD.AbrirConexion();

                BD.EjecutarConsulta(query);

                if (BD.ResultadoConsulta.Read())
                {

                    image.ImageUrl = carpeta + BD.ResultadoConsulta.GetString(posicion);

                }

                BD.CerrarConexion();

            }



            catch (Exception ex)
            {

                throw new Exception("Error al intentar Buscar la Foto" + ex.Message);



            }

        }



        public void LlenarGridviewPage(GridView grid, string Tabla, string cadenatributos, string condicion)
        {



            CsMysql BD = new CsMysql();

            string query = "select " + cadenatributos + " from " + Tabla;

            if (condicion != null && condicion.Length > 1)
            {

                query += " where " + condicion;

            }



            string cone = BD.Conexion.ConnectionString;

            MySqlConnection cnn = new MySqlConnection(cone);

            MySqlDataAdapter da = new MySqlDataAdapter(query, cone);

            System.Data.DataSet ds = new System.Data.DataSet();

            da.Fill(ds);

            grid.DataSource = ds;

            grid.DataBind();

            BD.CerrarConexion();



        }
    }
}