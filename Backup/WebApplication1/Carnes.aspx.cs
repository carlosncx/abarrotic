﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using Microsoft.Office.Interop.Word;

namespace WebApplication1
{
    public partial class Carnes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Lbl_Fecha.Text = DateTime.Now.ToString("dddd dd  MMMM");
            if (Session["sNomUsuario"] != null)
            {
                Lbl_Usuario.Text = Session["sNomUsuario"].ToString();
            }
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Lacteos.aspx");
        }

        protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Damas.aspx");
        }

        protected void ImageButton3_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Caballeros.aspx");
        }

        protected void ImageButton4_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void ImageButton6_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Perfumes.aspx");
        }

        protected void ImageButton7_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void ImageButton8_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Zapateria.aspx");
        }

        protected void ImageButton12_Click(object sender, ImageClickEventArgs e)
        {
            if (MessageBox.Show("" + Lbl_Producto.Text + " " + Lbl_Precio.Text + "$  ¿Quiere Agregar Este Producto A Su Compra?   " + "", "", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {

                try
                {
                    Clases.CsVentaproducto Venta = new Clases.CsVentaproducto();


                    Venta.producto = Lbl_Producto.Text;
                    Venta.precio = Lbl_Precio.Text;
                    Venta.usuario = Lbl_Usuario.Text;
                    Venta.fecha_venta = Lbl_Fecha.Text;

                    Venta.Guardar();
                    MessageBox.Show("Ingresado Correctamente");


                    
                }
                catch (Exception ex)
                {

                    Response.Write(ex.Message);

                }
            }
        }

        protected void ImageButton27_Click(object sender, ImageClickEventArgs e)
        {
            if (MessageBox.Show("" + Lbl_Producto2.Text + " " + Lbl_Precio2.Text + "$  ¿Quiere Agregar Este Producto A Su Compra?   " + "", "", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {

                try
                {
                    Clases.CsVentaproducto Venta = new Clases.CsVentaproducto();


                    Venta.producto = Lbl_Producto2.Text;
                    Venta.precio = Lbl_Precio2.Text;
                    Venta.usuario = Lbl_Usuario.Text;
                    Venta.fecha_venta = Lbl_Fecha.Text;

                    Venta.Guardar();
                    MessageBox.Show("Ingresado Correctamente");


                   
                }
                catch (Exception ex)
                {

                    Response.Write(ex.Message);

                }
            }
        }

        protected void ImageButton28_Click(object sender, ImageClickEventArgs e)
        {
            if (MessageBox.Show("P" + Lbl_Producto3.Text + " " + Lbl_Precio3.Text + "$  ¿Quiere Agregar Este Producto A Su Compra?   " + "", "", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {

                try
                {
                    Clases.CsVentaproducto Venta = new Clases.CsVentaproducto();


                    Venta.producto = Lbl_Producto3.Text;
                    Venta.precio = Lbl_Precio3.Text;
                    Venta.usuario = Lbl_Usuario.Text;
                    Venta.fecha_venta = Lbl_Fecha.Text;

                    Venta.Guardar();
                    MessageBox.Show("Ingresado Correctamente");


                   
                }
                catch (Exception ex)
                {

                    Response.Write(ex.Message);

                }
            }
        }

        protected void ImageButton29_Click(object sender, ImageClickEventArgs e)
        {
            if (MessageBox.Show("" + Lbl_Producto4.Text + " " + Lbl_Precio4.Text + "$  ¿Quiere Agregar Este Producto A Su Compra?   " + "", "", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {

                try
                {
                    Clases.CsVentaproducto Venta = new Clases.CsVentaproducto();


                    Venta.producto = Lbl_Producto4.Text;
                    Venta.precio = Lbl_Precio4.Text;
                    Venta.usuario = Lbl_Usuario.Text;
                    Venta.fecha_venta = Lbl_Fecha.Text;

                    Venta.Guardar();
                    MessageBox.Show("Ingresado Correctamente");


                   
                }
                catch (Exception ex)
                {

                    Response.Write(ex.Message);

                }
            }
        }

        protected void ImageButton13_Click(object sender, ImageClickEventArgs e)
        {
            if (MessageBox.Show("" + Lbl_Producto5.Text + " " + Lbl_Precio5.Text + "$  ¿Quiere Agregar Este Producto A Su Compra?   " + "", "", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {

                try
                {
                    Clases.CsVentaproducto Venta = new Clases.CsVentaproducto();


                    Venta.producto = Lbl_Producto5.Text;
                    Venta.precio = Lbl_Precio5.Text;
                    Venta.usuario = Lbl_Usuario.Text;
                    Venta.fecha_venta = Lbl_Fecha.Text;

                    Venta.Guardar();
                    MessageBox.Show("Ingresado Correctamente");


                    
                }
                catch (Exception ex)
                {

                    Response.Write(ex.Message);

                }
            }
        }

        protected void ImageButton24_Click(object sender, ImageClickEventArgs e)
        {
            if (MessageBox.Show("" + Lbl_Producto6.Text + " " + Lbl_Precio6.Text + "$  ¿Quiere Agregar Este Producto A Su Compra?   " + "", "", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {

                try
                {
                    Clases.CsVentaproducto Venta = new Clases.CsVentaproducto();


                    Venta.producto = Lbl_Producto6.Text;
                    Venta.precio = Lbl_Precio6.Text;
                    Venta.usuario = Lbl_Usuario.Text;
                    Venta.fecha_venta = Lbl_Fecha.Text;

                    Venta.Guardar();
                    MessageBox.Show("Ingresado Correctamente");


                    
                }
                catch (Exception ex)
                {

                    Response.Write(ex.Message);

                }
            }
        }

        protected void ImageButton25_Click(object sender, ImageClickEventArgs e)
        {
            if (MessageBox.Show("" + Lbl_Producto7.Text + " " + Lbl_Precio7.Text + "$  ¿Quiere Agregar Este Producto A Su Compra?   " + "", "", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {

                try
                {
                    Clases.CsVentaproducto Venta = new Clases.CsVentaproducto();


                    Venta.producto = Lbl_Producto7.Text;
                    Venta.precio = Lbl_Precio7.Text;
                    Venta.usuario = Lbl_Usuario.Text;
                    Venta.fecha_venta = Lbl_Fecha.Text;

                    Venta.Guardar();
                    MessageBox.Show("Ingresado Correctamente");


                   
                }
                catch (Exception ex)
                {

                    Response.Write(ex.Message);

                }
            }
        }

        protected void ImageButton26_Click(object sender, ImageClickEventArgs e)
        {
            if (MessageBox.Show("T" + Lbl_Producto8.Text + " " + Lbl_Precio8.Text + "$  ¿Quiere Agregar Este Producto A Su Compra?   " + "", "", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {

                try
                {
                    Clases.CsVentaproducto Venta = new Clases.CsVentaproducto();


                    Venta.producto = Lbl_Producto8.Text;
                    Venta.precio = Lbl_Precio8.Text;
                    Venta.usuario = Lbl_Usuario.Text;
                    Venta.fecha_venta = Lbl_Fecha.Text;

                    Venta.Guardar();
                    MessageBox.Show("Ingresado Correctamente");


                    
                }
                catch (Exception ex)
                {

                    Response.Write(ex.Message);

                }
            }
        }

        protected void ImageButton30_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void ImageButton31_Click(object sender, ImageClickEventArgs e)
        {

           
        }

        protected void ImageButton32_Click(object sender, ImageClickEventArgs e)
        {
           
        }

        protected void ImageButton33_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void ImageButton18_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Damas.aspx");
        }

        protected void ImageButton19_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Caballeros.aspx");
        }

        protected void ImageButton20_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void ImageButton21_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Perfumes.aspx");
        }

        protected void ImageButton22_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void ImageButton23_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Zapateria.aspx");
        }
    }
}