﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class Administradores : System.Web.UI.Page
    {
        int casa;
        Clases.csHerramientas llena = new Clases.csHerramientas();
        string foto;
        protected void Page_Load(object sender, EventArgs e)
        {
            llena.LlenarGridquery(gvadministradores, "select * from administradores");
        }

        protected void ImageGuardar_Click(object sender, ImageClickEventArgs e)
        {
            if (fuFoto.HasFile)
            {
                string fullPath = Path.Combine(Server.MapPath("/fotos/"), llena.GenerarNombreArchivo(fuFoto));
                fuFoto.SaveAs(fullPath);
                foto = Path.GetFileName(fullPath);
            }

            llena.guardar_admin(txtUsuario.Text, txtClave.Text, foto);
            Response.Redirect("Administradores.aspx");
        }

        protected void ImageModificar_Click(object sender, ImageClickEventArgs e)
        {
            casa = Convert.ToInt32(txtID.Text);

            Clases.CsAdministradores adm = new Clases.CsAdministradores();

            try
            {

                adm.Id_admn = this.casa;
                adm.Username = txtUsuario.Text;
                adm.Password = txtClave.Text;
                adm.Direccion_fot = txtDireccion.Text;

                //Si selecciono un archivo


                adm.Modificar();


                Response.Write("Registro Modificado");


                Response.Redirect("Administradores.aspx");

            }

            catch (Exception ex)
            {

                Response.Write(ex.Message);



            }
        }

        protected void ImageEliminar_Click(object sender, ImageClickEventArgs e)
        {
            casa = Convert.ToInt32(txtID.Text);


            Clases.CsAdministradores adm = new Clases.CsAdministradores();
            try
            {



                adm.Eliminar(this.casa);

                Response.Write("Registro Eliminado!");

                txtID.Text = "";

                txtUsuario.Text = "";

                txtClave.Text = "";

                Response.Redirect("Administradores.aspx");

            }

            catch (Exception ex)
            {

                Response.Write(ex.Message);

            }
        }

        protected void gvProductos_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtID.Text = gvadministradores.Rows[gvadministradores.SelectedIndex].Cells[0].Text;
            txtUsuario.Text = gvadministradores.Rows[gvadministradores.SelectedIndex].Cells[1].Text;
            txtClave.Text = gvadministradores.Rows[gvadministradores.SelectedIndex].Cells[2].Text;
            txtDireccion.Text = gvadministradores.Rows[gvadministradores.SelectedIndex].Cells[4].Text;
        }
    }
}