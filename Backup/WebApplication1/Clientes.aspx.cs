﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class Clientes : System.Web.UI.Page
    {

        int casa;
        Clases.CsClientes cli = new Clases.CsClientes();
        Clases.csHerramientas herra = new Clases.csHerramientas();

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                Clases.CsClientes cli = new Clases.CsClientes();


                cli.Nombre_cli = txtnomb_cli.Text;
                cli.Apellido_pat = txtape_pat.Text;
                cli.Apellido_mat = txtape_mat.Text;
                cli.Calles_cli = txtcalles.Text;
                cli.Num_casa = txtnum_casa.Text;
                cli.Colonia_cli = txtcolonia.Text;
                cli.Ciudad_cli = txtciudad.Text;               
                cli.Telefono_cli = txttelefono.Text;
                cli.Codigo_postal_cli = txtcod_postal.Text;

                //cli.Foto = tooli.GenerarNombreArchivo(fufotocasa);



                if (cli.Guardar())
                {

    
                }

                Response.Write("Grabado con Exito!");

                txtnomb_cli.Text = "";
                txtape_pat.Text = "";
                txtape_mat.Text = "";
                txtcalles.Text = "";
                txtnum_casa.Text = "";
                txtciudad.Text = "";
                txtcolonia.Text = "";
                txttelefono.Text = "";
                txtcod_postal.Text = "";
                Response.Redirect("Clientes.aspx");
            }
            catch (Exception ex)
            {

                Response.Write(ex.Message);

            }
        }

        
  

        protected void Page_Load(object sender, EventArgs e)
        {
            herra.LlenarGridquery(gvClientes, "select * from clientes");
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtidcli.Text = gvClientes.Rows[gvClientes.SelectedIndex].Cells[0].Text;
            txtnomb_cli.Text = gvClientes.Rows[gvClientes.SelectedIndex].Cells[1].Text;
            txtape_pat.Text = gvClientes.Rows[gvClientes.SelectedIndex].Cells[2].Text;
            txtape_mat.Text = gvClientes.Rows[gvClientes.SelectedIndex].Cells[3].Text;
            txtcalles.Text = gvClientes.Rows[gvClientes.SelectedIndex].Cells[4].Text;
            txtnum_casa.Text = gvClientes.Rows[gvClientes.SelectedIndex].Cells[5].Text;
            txtcolonia.Text = gvClientes.Rows[gvClientes.SelectedIndex].Cells[6].Text;
            txtciudad.Text = gvClientes.Rows[gvClientes.SelectedIndex].Cells[7].Text;
            txttelefono.Text = gvClientes.Rows[gvClientes.SelectedIndex].Cells[8].Text;
            txtcod_postal.Text = gvClientes.Rows[gvClientes.SelectedIndex].Cells[9].Text;
        }

        protected void ImageButton3_Click(object sender, ImageClickEventArgs e)
        {
            casa = Convert.ToInt32(txtidcli.Text);

            Clases.CsClientes cli = new Clases.CsClientes();

            try
            {

                cli.Id_cli = this.casa;
                cli.Nombre_cli = txtnomb_cli.Text;
                cli.Apellido_mat =txtape_mat.Text;
                cli.Apellido_pat = txtape_pat.Text;
                cli.Calles_cli = txtcalles.Text;
                cli.Num_casa = txtnum_casa.Text;
                cli.Colonia_cli = txtcolonia.Text;
                cli.Ciudad_cli = txtciudad.Text;
                cli.Telefono_cli = txttelefono.Text;
                cli.Codigo_postal_cli = txtcod_postal.Text;
                //Si selecciono un archivo


                cli.Modificar();


                Response.Write("Registro Modificado");

                Response.Redirect("Clientes.aspx");


            }

            catch (Exception ex)
            {

                Response.Write(ex.Message);

            }
        }

        protected void ImageButton4_Click(object sender, ImageClickEventArgs e)
        {
            //Leer las variables

            casa = Convert.ToInt32(txtidcli.Text);


           Clases.CsClientes cli = new Clases.CsClientes();
            try
            {

                cli.Eliminar(this.casa);
                Response.Write("Registro Eliminado!");
                txtnomb_cli.Text = "";
                txtape_pat.Text = "";
                txtape_mat.Text = "";
                txtcalles.Text = "";
                txtnum_casa.Text = "";
                txtciudad.Text = "";
                txtcolonia.Text = "";
                txttelefono.Text = "";
                txtcod_postal.Text = "";
                Response.Redirect("Clientes.aspx");

            }

            catch (Exception ex)
            {

                Response.Write(ex.Message);

            }
        }
    }
}