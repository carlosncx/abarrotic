﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using Microsoft.Office.Interop.Word;

namespace WebApplication1
{
    public partial class Lacteos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Lbl_Fecha.Text = DateTime.Now.ToString("dddd dd  MMMM");
            if (Session["sNomUsuario"] != null)
            {
                Lbl_Usuario.Text = Session["sNomUsuario"].ToString();
            }

            if (!(IsPostBack))
            {
                //Response.Write("Caraga");
                //DropDownList1.DataTextField = "1/2 Kilo";
                //DropDownList1.DataTextField = "1/4 De Kilo";
                //DropDownList1.DataSource = GetStuff();
                //DropDownList1.DataBind();
                //DropDownList1.Items.Insert(0, "--Seleccione una Cantidad--");
            }
        }

        protected void ImageButton25_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Carnes.aspx");
        }

        protected void ImageButton26_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Damas.aspx");
        }

        protected void ImageButton27_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Caballeros.aspx");
        }

        protected void ImageButton28_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void ImageButton29_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Perfumes.aspx");
        }

        protected void ImageButton30_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void ImageButton31_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Zapateria.aspx");
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            if (MessageBox.Show("" + Lbl_Producto.Text + " " + Lbl_Precio.Text + "$  ¿Quiere Agregar Este Producto A Su Compra?   " + "", "", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {

                try
                {
                    Clases.CsVentaproducto Venta = new Clases.CsVentaproducto();
                    Venta.producto = Lbl_Producto.Text;
                    Venta.precio = Lbl_Precio.Text;
                    Venta.usuario = Lbl_Usuario.Text;
                    Venta.fecha_venta = Lbl_Fecha.Text;

                    Venta.Guardar();
                    MessageBox.Show("Ingresado Correctamente");

                   
                }
                catch (Exception ex)
                {

                    Response.Write(ex.Message);

                }
            }
        }

        protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
        {
            if (MessageBox.Show("" + Lbl_Producto1.Text + " " + Lbl_Precio0.Text + "$  ¿Quiere Agregar Este Producto A Su Compra?   " + "", "", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {

                try
                {
                    Clases.CsVentaproducto Venta = new Clases.CsVentaproducto();
                    Venta.producto = Lbl_Producto1.Text;
                    Venta.precio = Lbl_Precio0.Text;
                    Venta.usuario = Lbl_Usuario.Text;
                    Venta.fecha_venta = Lbl_Fecha.Text;

                    Venta.Guardar();
                    MessageBox.Show("Ingresado Correctamente");

                 
                }
                catch (Exception ex)
                {

                    Response.Write(ex.Message);

                }
            }
        }

        protected void ImageButton3_Click(object sender, ImageClickEventArgs e)
        {
            if (MessageBox.Show("" + Lbl_Producto2.Text + " " + Lbl_Precio1.Text + "$  ¿Quiere Agregar Este Producto A Su Compra?   " + "", "", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {

                try
                {
                    Clases.CsVentaproducto Venta = new Clases.CsVentaproducto();
                    Venta.producto = Lbl_Producto2.Text;
                    Venta.precio = Lbl_Precio1.Text;
                    Venta.usuario = Lbl_Usuario.Text;
                    Venta.fecha_venta = Lbl_Fecha.Text;

                    Venta.Guardar();
                    MessageBox.Show("Ingresado Correctamente");

                  
                }
                catch (Exception ex)
                {

                    Response.Write(ex.Message);

                }
            }
        }

        protected void ImageButton4_Click(object sender, ImageClickEventArgs e)
        {
            if (MessageBox.Show("" + Lbl_Producto3.Text + " " + Lbl_Precio2.Text + "$  ¿Quiere Agregar Este Producto A Su Compra?   " + "", "", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {

                try
                {
                    Clases.CsVentaproducto Venta = new Clases.CsVentaproducto();
                    Venta.producto = Lbl_Producto3.Text;
                    Venta.precio = Lbl_Precio2.Text;
                    Venta.usuario = Lbl_Usuario.Text;
                    Venta.fecha_venta = Lbl_Fecha.Text;

                    Venta.Guardar();
                    MessageBox.Show("Ingresado Correctamente");

                  
                }
                catch (Exception ex)
                {

                    Response.Write(ex.Message);

                }
            }
        }

        protected void ImageButton8_Click(object sender, ImageClickEventArgs e)
        {
            if (MessageBox.Show("" + Lbl_Producto4.Text + " " + Lbl_Precio3.Text + "$  ¿Quiere Agregar Este Producto A Su Compra?   " + "", "", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {

                try
                {
                    Clases.CsVentaproducto Venta = new Clases.CsVentaproducto();
                    Venta.producto = Lbl_Producto4.Text;
                    Venta.precio = Lbl_Precio3.Text;
                    Venta.usuario = Lbl_Usuario.Text;
                    Venta.fecha_venta = Lbl_Fecha.Text;

                    Venta.Guardar();
                    MessageBox.Show("Ingresado Correctamente");

                    
                }
                catch (Exception ex)
                {

                    Response.Write(ex.Message);

                }
            }
        }

        protected void ImageButton7_Click(object sender, ImageClickEventArgs e)
        {
            if (MessageBox.Show("" + Lbl_Producto5.Text + " " + Lbl_Precio4.Text + "$  ¿Quiere Agregar Este Producto A Su Compra?   " + "", "", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {

                try
                {
                    Clases.CsVentaproducto Venta = new Clases.CsVentaproducto();
                    Venta.producto = Lbl_Producto5.Text;
                    Venta.precio = Lbl_Precio4.Text;
                    Venta.usuario = Lbl_Usuario.Text;
                    Venta.fecha_venta = Lbl_Fecha.Text;

                    Venta.Guardar();
                    MessageBox.Show("Ingresado Correctamente");

                    
                }
                catch (Exception ex)
                {

                    Response.Write(ex.Message);

                }
            }
        }

        protected void ImageButton6_Click(object sender, ImageClickEventArgs e)
        {
            if (MessageBox.Show("" + Lbl_Producto6.Text + " " + Lbl_Precio5.Text + "$  ¿Quiere Agregar Este Producto A Su Compra?   " + "", "", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {

                try
                {
                    Clases.CsVentaproducto Venta = new Clases.CsVentaproducto();
                    Venta.producto = Lbl_Producto6.Text;
                    Venta.precio = Lbl_Precio5.Text;
                    Venta.usuario = Lbl_Usuario.Text;
                    Venta.fecha_venta = Lbl_Fecha.Text;

                    Venta.Guardar();
                    MessageBox.Show("Ingresado Correctamente");

                    
                }
                catch (Exception ex)
                {

                    Response.Write(ex.Message);

                }
            }
        }

        protected void ImageButton5_Click(object sender, ImageClickEventArgs e)
        {
            if (MessageBox.Show("" + Lbl_Producto7.Text + " " + Lbl_Precio6.Text + "$  ¿Quiere Agregar Este Producto A Su Compra?   " + "", "", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {

                try
                {
                    Clases.CsVentaproducto Venta = new Clases.CsVentaproducto();
                    Venta.producto = Lbl_Producto7.Text;
                    Venta.precio = Lbl_Precio6.Text;
                    Venta.usuario = Lbl_Usuario.Text;
                    Venta.fecha_venta = Lbl_Fecha.Text;

                    Venta.Guardar();
                    MessageBox.Show("Ingresado Correctamente");

                  
                }
                catch (Exception ex)
                {

                    Response.Write(ex.Message);

                }
            }
        }

        protected void ImageButton17_Click(object sender, ImageClickEventArgs e)
        {
            if (MessageBox.Show("" + Lbl_Producto8.Text + " " + Lbl_Precio7.Text + "$ ¿Quiere Agregar Este Producto A Su Compra?   " + "", "", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {

                try
                {
                    Clases.CsVentaproducto Venta = new Clases.CsVentaproducto();
                    Venta.producto = Lbl_Producto8.Text;
                    Venta.precio = Lbl_Precio7.Text;
                    Venta.usuario = Lbl_Usuario.Text;
                    Venta.fecha_venta = Lbl_Fecha.Text;

                    Venta.Guardar();
                    MessageBox.Show("Ingresado Correctamente");

                   
                }
                catch (Exception ex)
                {

                    Response.Write(ex.Message);

                }
            }
        }

        protected void ImageButton18_Click(object sender, ImageClickEventArgs e)
        {
            if (MessageBox.Show("" + Lbl_Producto9.Text + " " + Lbl_Precio8.Text + " $  ¿Quiere Agregar Este Producto A Su Compra?   " + "", "", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {

                try
                {
                    Clases.CsVentaproducto Venta = new Clases.CsVentaproducto();
                    Venta.producto = Lbl_Producto9.Text;
                    Venta.precio = Lbl_Precio8.Text;
                    Venta.usuario = Lbl_Usuario.Text;
                    Venta.fecha_venta = Lbl_Fecha.Text;

                    Venta.Guardar();
                    MessageBox.Show("Ingresado Correctamente");

                    
                }
                catch (Exception ex)
                {

                    Response.Write(ex.Message);

                }
            }
        }

        protected void ImageButton19_Click(object sender, ImageClickEventArgs e)
        {
            if (MessageBox.Show("" + Lbl_Producto10.Text + " " + Lbl_Precio9.Text + " $  ¿Quiere Agregar Este Producto A Su Compra?   " + "", "", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {

                try
                {
                    Clases.CsVentaproducto Venta = new Clases.CsVentaproducto();
                    Venta.producto = Lbl_Producto10.Text;
                    Venta.precio = Lbl_Precio9.Text;
                    Venta.usuario = Lbl_Usuario.Text;
                    Venta.fecha_venta = Lbl_Fecha.Text;

                    Venta.Guardar();
                    MessageBox.Show("Ingresado Correctamente");

                   
                }
                catch (Exception ex)
                {

                    Response.Write(ex.Message);

                }
            }
        }

        protected void ImageButton20_Click(object sender, ImageClickEventArgs e)
        {
            if (MessageBox.Show("" + Lbl_Producto11.Text + " " + Lbl_Precio10.Text + "$  ¿Quiere Agregar Este Producto A Su Compra?   " + "", "", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {

                try
                {
                    Clases.CsVentaproducto Venta = new Clases.CsVentaproducto();
                    Venta.producto = Lbl_Producto11.Text;
                    Venta.precio = Lbl_Precio10.Text;
                    Venta.usuario = Lbl_Usuario.Text;
                    Venta.fecha_venta = Lbl_Fecha.Text;

                    Venta.Guardar();
                    MessageBox.Show("Ingresado Correctamente");

                   
                }
                catch (Exception ex)
                {

                    Response.Write(ex.Message);

                }
            }
        }

        protected void ImageButton24_Click(object sender, ImageClickEventArgs e)
        {
            if (MessageBox.Show("" + Lbl_Producto12.Text + " " + Lbl_Precio11.Text + "$  ¿Quiere Agregar Este Producto A Su Compra?   " + "", "", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {

                try
                {
                    Clases.CsVentaproducto Venta = new Clases.CsVentaproducto();
                    Venta.producto = Lbl_Producto12.Text;
                    Venta.precio = Lbl_Precio11.Text;
                    Venta.usuario = Lbl_Usuario.Text;
                    Venta.fecha_venta = Lbl_Fecha.Text;

                    Venta.Guardar();
                    MessageBox.Show("Ingresado Correctamente");

                  
                }
                catch (Exception ex)
                {

                    Response.Write(ex.Message);

                }
            }
        }

        protected void ImageButton23_Click(object sender, ImageClickEventArgs e)
        {
            if (MessageBox.Show("" + Lbl_Producto13.Text + " " + Lbl_Precio12.Text + "$  ¿Quiere Agregar Este Producto A Su Compra?   " + "", "", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {

                try
                {
                    Clases.CsVentaproducto Venta = new Clases.CsVentaproducto();
                    Venta.producto = Lbl_Producto13.Text;
                    Venta.precio = Lbl_Precio12.Text;
                    Venta.usuario = Lbl_Usuario.Text;
                    Venta.fecha_venta = Lbl_Fecha.Text;

                    Venta.Guardar();
                    MessageBox.Show("Ingresado Correctamente");

                  
                }
                catch (Exception ex)
                {

                    Response.Write(ex.Message);

                }
            }
        }

        protected void ImageButton22_Click(object sender, ImageClickEventArgs e)
        {
            if (MessageBox.Show("" + Lbl_Producto14.Text + " " + Lbl_Precio13.Text + "$  ¿Quiere Agregar Este Producto A Su Compra?   " + "", "", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {

                try
                {
                    Clases.CsVentaproducto Venta = new Clases.CsVentaproducto();
                    Venta.producto = Lbl_Producto14.Text;
                    Venta.precio = Lbl_Precio13.Text;
                    Venta.usuario = Lbl_Usuario.Text;
                    Venta.fecha_venta = Lbl_Fecha.Text;

                    Venta.Guardar();
                    MessageBox.Show("Ingresado Correctamente");

                    
                }
                catch (Exception ex)
                {

                    Response.Write(ex.Message);

                }
            }
        }

        protected void ImageButton21_Click(object sender, ImageClickEventArgs e)
        {
            if (MessageBox.Show("" + Lbl_Producto15.Text + " " + Lbl_Precio14.Text + "$  ¿Quiere Agregar Este Producto A Su Compra?   " + "", "", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {

                try
                {
                    Clases.CsVentaproducto Venta = new Clases.CsVentaproducto();
                    Venta.producto = Lbl_Producto15.Text;
                    Venta.precio = Lbl_Precio14.Text;
                    Venta.usuario = Lbl_Usuario.Text;
                    Venta.fecha_venta = Lbl_Fecha.Text;

                    Venta.Guardar();
                    MessageBox.Show("Ingresado Correctamente");

                    
                }
                catch (Exception ex)
                {

                    Response.Write(ex.Message);

                }
            }
        }

        protected void ImageButton34_Click(object sender, ImageClickEventArgs e)
        {
            if (MessageBox.Show("" + Lbl_Producto16.Text + " " + Lbl_Precio15.Text + "$  ¿Quiere Agregar Este Producto A Su Compra?   " + "", "", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {

                try
                {
                    Clases.CsVentaproducto Venta = new Clases.CsVentaproducto();
                    Venta.producto = Lbl_Producto16.Text;
                    Venta.precio = Lbl_Precio15.Text;
                    Venta.usuario = Lbl_Usuario.Text;
                    Venta.fecha_venta = Lbl_Fecha.Text;

                    Venta.Guardar();
                    MessageBox.Show("Ingresado Correctamente");

                   
                }
                catch (Exception ex)
                {

                    Response.Write(ex.Message);

                }
            }
        }

        protected void ImageButton33_Click(object sender, ImageClickEventArgs e)
        {
            if (MessageBox.Show("" + Lbl_Producto17.Text + " " + Lbl_Precio16.Text + "$  ¿Quiere Agregar Este Producto A Su Compra?   " + "", "", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {

                try
                {
                    Clases.CsVentaproducto Venta = new Clases.CsVentaproducto();
                    Venta.producto = Lbl_Producto17.Text;
                    Venta.precio = Lbl_Precio16.Text;
                    Venta.usuario = Lbl_Usuario.Text;
                    Venta.fecha_venta = Lbl_Fecha.Text;

                    Venta.Guardar();
                    MessageBox.Show("Ingresado Correctamente");

                  
                }
                catch (Exception ex)
                {

                    Response.Write(ex.Message);

                }
            }
        }

        protected void ImageButton14_Click(object sender, ImageClickEventArgs e)
        {
            if (MessageBox.Show("" + Lbl_Producto18.Text + " " + Lbl_Precio17.Text + "$  ¿Quiere Agregar Este Producto A Su Compra?   " + "", "", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {

                try
                {
                    Clases.CsVentaproducto Venta = new Clases.CsVentaproducto();
                    Venta.producto = Lbl_Producto18.Text;
                    Venta.precio = Lbl_Precio17.Text;
                    Venta.usuario = Lbl_Usuario.Text;
                    Venta.fecha_venta = Lbl_Fecha.Text;

                    Venta.Guardar();
                    MessageBox.Show("Ingresado Correctamente");

                    
                }
                catch (Exception ex)
                {

                    Response.Write(ex.Message);

                }
            }
        }

        protected void ImageButton13_Click(object sender, ImageClickEventArgs e)
        {
            if (MessageBox.Show("" + Lbl_Producto19.Text + " " + Lbl_Precio18.Text + "$  ¿Quiere Agregar Este Producto A Su Compra?   " + "", "", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {

                try
                {
                    Clases.CsVentaproducto Venta = new Clases.CsVentaproducto();
                    Venta.producto = Lbl_Producto19.Text;
                    Venta.precio = Lbl_Precio18.Text;
                    Venta.usuario = Lbl_Usuario.Text;
                    Venta.fecha_venta = Lbl_Fecha.Text;

                    Venta.Guardar();
                    MessageBox.Show("Ingresado Correctamente");

                    
                }
                catch (Exception ex)
                {

                    Response.Write(ex.Message);

                }
            }
        }

        protected void ImageButton10_Click(object sender, ImageClickEventArgs e)
        {
            if (MessageBox.Show("" + Lbl_Producto20.Text + " " + Lbl_Precio19.Text + "$  ¿Quiere Agregar Este Producto A Su Compra?   " + "", "", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {

                try
                {
                    Clases.CsVentaproducto Venta = new Clases.CsVentaproducto();
                    Venta.producto = Lbl_Producto20.Text;
                    Venta.precio = Lbl_Precio19.Text;
                    Venta.usuario = Lbl_Usuario.Text;
                    Venta.fecha_venta = Lbl_Fecha.Text;

                    Venta.Guardar();
                    MessageBox.Show("Ingresado Correctamente");

                   
                }
                catch (Exception ex)
                {

                    Response.Write(ex.Message);

                }
            }
        }

        protected void ImageButton9_Click(object sender, ImageClickEventArgs e)
        {
            if (MessageBox.Show("" + Lbl_Producto21.Text + " " + Lbl_Precio20.Text + "$  ¿Quiere Agregar Este Producto A Su Compra?   " + "", "", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {

                try
                {
                    Clases.CsVentaproducto Venta = new Clases.CsVentaproducto();
                    Venta.producto = Lbl_Producto21.Text;
                    Venta.precio = Lbl_Precio20.Text;
                    Venta.usuario = Lbl_Usuario.Text;
                    Venta.fecha_venta = Lbl_Fecha.Text;

                    Venta.Guardar();
                    MessageBox.Show("Ingresado Correctamente");

                   
                }
                catch (Exception ex)
                {

                    Response.Write(ex.Message);

                }
            }
        }

        protected void ImageButton12_Click(object sender, ImageClickEventArgs e)
        {
            if (MessageBox.Show("" + Lbl_Producto22.Text + " " + Lbl_Precio21.Text + "$  ¿Quiere Agregar Este Producto A Su Compra?   " + "", "", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {

                try
                {
                    Clases.CsVentaproducto Venta = new Clases.CsVentaproducto();
                    Venta.producto = Lbl_Producto22.Text;
                    Venta.precio = Lbl_Precio21.Text;
                    Venta.usuario = Lbl_Usuario.Text;
                    Venta.fecha_venta = Lbl_Fecha.Text;

                    Venta.Guardar();
                    MessageBox.Show("Ingresado Correctamente");

                  
                }
                catch (Exception ex)
                {

                    Response.Write(ex.Message);

                }
            }
        }

        protected void ImageButton11_Click(object sender, ImageClickEventArgs e)
        {
            if (MessageBox.Show("" + Lbl_Producto23.Text + " " + Lbl_Precio2.Text + "$  ¿Quiere Agregar Este Producto A Su Compra?   " + "", "", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {

                try
                {
                    Clases.CsVentaproducto Venta = new Clases.CsVentaproducto();
                    Venta.producto = Lbl_Producto23.Text;
                    Venta.precio = Lbl_Precio22.Text;
                    Venta.usuario = Lbl_Usuario.Text;
                    Venta.fecha_venta = Lbl_Fecha.Text;

                    Venta.Guardar();
                    MessageBox.Show("Ingresado Correctamente");

                   
                }
                catch (Exception ex)
                {

                    Response.Write(ex.Message);

                }
            }
        }

        protected void ImageButton35_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void ImageButton36_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void ImageButton37_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void ImageButton45_Click2(object sender, ImageClickEventArgs e)
        {

        }

        protected void ImageButton38_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Carnes.aspx");
        }

        protected void ImageButton39_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Damas.aspx");
        }

        protected void ImageButton40_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Caballeros.aspx");
        }

        protected void ImageButton41_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void ImageButton42_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Perfumes.aspx");
        }

        protected void ImageButton43_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void ImageButton44_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Zapateria.aspx");
        }
    }
}