﻿using System;
using System.Collections.Generic;
using System.Web;

namespace WebApplication1.Clases
{
    public class CsClientes
    {

        // Declarar Atributos

        private int id_cli;

        private string nombre_cli;
        private string apellido_pat;
        private string apellido_mat;
        private string calles_cli;
        private string colonia_cli;
        private string ciudad_cli;
        private string num_casa;
        private string telefono_cli;
        private string codigo_postal_cli;



        // Propiedades



        public int Id_cli
        {

            get { return this.id_cli; }

            set { this.id_cli = value; }

        }

        
        public string Nombre_cli
        {

            get { return this.nombre_cli; }

            set { this.nombre_cli = value; }
            
        }
        public string Apellido_pat
        {

            get { return this.apellido_pat; }

            set { this.apellido_pat = value; }

        }

        public string Apellido_mat
        {

            get { return this.apellido_mat; }

            set { this.apellido_mat = value; }

        }
        public string Calles_cli
        {

            get { return this.calles_cli; }

            set { this.calles_cli = value; }

        }
        public string Num_casa
        {

            get { return this.num_casa; }

            set { this.num_casa= value; }

        }
        public string Colonia_cli
        {

            get { return this.colonia_cli; }

            set { this.colonia_cli = value; }

        }

        
        public string Ciudad_cli
        {

            get { return this.ciudad_cli; }

            set { this.ciudad_cli= value; }
            
        }



        public string Telefono_cli
        {

            get { return this.telefono_cli; }

            set { this.telefono_cli= value; }
            
        }


        public string Codigo_postal_cli
        {

            get { return this.codigo_postal_cli; }

            set { this.codigo_postal_cli = value; }

        }

        
        public bool Guardar()
        {

            string query = "insert into clientes (nombre_cli,apellido_pat,apellido_mat,calles_cli,num_casa,colonia_cli,ciudad_cli,telefono_cli,codigo_postal_cli) values ";

            query += "('" + this.nombre_cli + "', '";
            query += this.apellido_pat+ "', '";
            query += this.apellido_mat + "', '";
            query += this.calles_cli + "', ";
            query += this.num_casa + ", '";
            query += this.colonia_cli + "', '";
            query += this.ciudad_cli + "','";
            query += this.telefono_cli + "','";
            query += this.codigo_postal_cli + "') ";
            

            
            try
            {



                CsMysql BD = new CsMysql();

                BD.AbrirConexion();

                BD.EjecutarSql(query);

                BD.CerrarConexion();

                return true;

            }

                
            catch (Exception ex)
            {

                throw new Exception("Error al intentar guardar el Registro" + ex.Message);
                
            }
            
        }



        public bool Modificar()
        {

            string query = "update clientes set ";

            query += "nombre_cli= '" + this.nombre_cli + "', ";
            query += "apellido_pat= '" + this.apellido_pat + "',";
            query += "apellido_mat= '" + this.apellido_mat + "',";
            query += "calles_cli = '" + this.calles_cli+ "',";
            query += "num_casa = '" + this.num_casa + "',";
            query += "colonia_cli = '" + this.colonia_cli + "',";
            query += "ciudad_cli = '" + this.ciudad_cli+ "',";
            query += "telefono_cli = '" + this.telefono_cli + "',";
            query += "codigo_postal_cli = '" + this.codigo_postal_cli + "'";
            query += " where id_cli = " + this.id_cli;

            try
            {

                CsMysql BD = new CsMysql();

                BD.AbrirConexion();

                BD.EjecutarSql(query);

                BD.CerrarConexion();

                return true;

            }

            catch (Exception ex)
            {

                throw new Exception("Error al intentar Modificar el Registro" + ex.Message);

            }

        }



        public bool Eliminar(int casa)
        {

            string query = "delete from clientes where id_cli = " + casa;

            try
            {

                CsMysql BD = new CsMysql();

                BD.AbrirConexion();

                BD.EjecutarSql(query);

                BD.CerrarConexion();

                return true;

            }


            catch (Exception ex)
            {

                throw new Exception("Error al intentar Eliminar Registro" + ex.Message);

            }

        }



        public bool Buscar(int cliente)
        {

            string query = "select * from clientes where id_cli = " + cliente;



            try
            {



                CsMysql BD = new CsMysql();

                BD.AbrirConexion();

                BD.EjecutarConsulta(query);

                if (BD.ResultadoConsulta.Read())
                {

                    this.id_cli = Convert.ToInt32(BD.ResultadoConsulta["id_cli"]);
                    this.nombre_cli = BD.ResultadoConsulta["nombre_cli"].ToString();
                    this.apellido_pat = BD.ResultadoConsulta["apellido_pat"].ToString();
                    this.apellido_mat = BD.ResultadoConsulta["apellido_mat"].ToString();
                    this.calles_cli = BD.ResultadoConsulta["calles_cli"].ToString();
                    this.num_casa = BD.ResultadoConsulta["num_casa"].ToString();
                    this.colonia_cli = BD.ResultadoConsulta["colonia_cli"].ToString();
                    this.ciudad_cli = BD.ResultadoConsulta["ciudad_cli"].ToString();
                    this.telefono_cli = BD.ResultadoConsulta["telefono_cli"].ToString();
                    this.codigo_postal_cli = BD.ResultadoConsulta["codigo_postal_cli"].ToString();

                   

                }

                BD.CerrarConexion();

                return true;

            }



            catch (Exception ex)
            {

                throw new Exception("Error al intentar Buscar el Registro" + ex.Message);



            }


        }
    }
}