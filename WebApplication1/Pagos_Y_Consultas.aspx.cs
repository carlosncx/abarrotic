﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using Microsoft.Office.Interop.Word;

namespace WebApplication1
{
    public partial class Pagos_Y_Consultas : System.Web.UI.Page
    {
        int casa;
        string usuario;
        string fecha;

        protected void Page_Load(object sender, EventArgs e)
        {

            Lbl_Fecha.Text = DateTime.Now.ToString("yyyy/MM/dd");

            if (Session["sNomUsuario"] != null)
            {
                Lbl_Usuario.Text = Session["sNomUsuario"].ToString();
            }

            MySqlConnection oConn = new MySqlConnection("Server=localhost;Database=abarrotic;Uid=root;Pwd=1234");
            try
            {
                oConn.Open();
                //Establecer el query a ejecutar
                string query = "select * from Ventasproductos where usuario like '%" + Lbl_Usuario.Text.Trim() + "%' and Fecha_Venta = '" + Lbl_Fecha.Text.Trim() + "'";
                MySqlCommand oComando = new MySqlCommand(query, oConn);
                DataSet ds = new DataSet();
                MySqlDataAdapter oAdapter = new MySqlDataAdapter(oComando);
                oAdapter.Fill(ds);


                Gr_Consultas.DataSource = ds.Tables[0];
                Gr_Consultas.DataBind();
                oConn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al consultar" + ex.Message);
            }
        }



        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {

            Txt_Id.Text = Gr_Consultas.Rows[Gr_Consultas.SelectedIndex].Cells[0].Text;
        }

        protected void btnCancelarprod_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Esta seguro(a) de eliminar este producto?   " + "", "", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {


                //Leer las variables

                casa = Convert.ToInt32(Txt_Id.Text);


                Clases.CsVentaproducto eliminar = new Clases.CsVentaproducto();
                try
                {

                    eliminar.Eliminar(this.casa);
                    MessageBox.Show("El producto a sido eliminado");
                    Txt_Id.Text = "";
                    Response.Redirect("Pagos_Y_Consultas.aspx");


                }

                catch (Exception ex)
                {

                    Response.Write(ex.Message);

                }
            }
        }

        protected void btnCancelarCompra_Click(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                Clases.CsClientes cli = new Clases.CsClientes();


                

                Response.Write("Grabado con Exito!");

             
                Response.Redirect("Clientes.aspx");
            }
            catch (Exception ex)
            {

                Response.Write(ex.Message);

            }
        }

        protected void btnEliminarprod_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Esta seguro(a) de eliminar este producto?   " + "", "", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {


                //Leer las variables

                casa = Convert.ToInt32(Txt_Id.Text);


                Clases.CsVentaproducto eliminar = new Clases.CsVentaproducto();
                try
                {

                    eliminar.Eliminar(this.casa);
                    MessageBox.Show("El producto a sido eliminado");
                    Txt_Id.Text = "";
                    Response.Redirect("Pagos_Y_Consultas.aspx");


                }

                catch (Exception ex)
                {

                    Response.Write(ex.Message);

                }
            }
        }

        protected void btnEliminarCompra_Click(object sender, EventArgs e)
        {
            MySqlConnection oConn = new MySqlConnection("Server=localhost;Database=abarrotic;Uid=root;Pwd=1234;");
            try
            {
                oConn.Open();
                if (MessageBox.Show("¿Esta Seguro(a) De Querer Eliminar toda su compra?    ", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    string query = "delete from Ventasproductos where usuario like '%" + Lbl_Usuario.Text.Trim() + "%' and Fecha_Venta = '" + Lbl_Fecha.Text.Trim() + "'";
                    MySqlCommand oComando = new MySqlCommand(query, oConn);
                    oComando.ExecuteNonQuery();

                    MessageBox.Show("los Datos Fueron Eliminados ");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al consultar" + ex.Message);
            }

            Response.Redirect("Pagos_Y_Consultas.aspx");
        }

        protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
        {
            Txt_Credito.Visible = true;
        }
    }
}