﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class Productos : System.Web.UI.Page
    {
        Clases.CsProductos producto = new Clases.CsProductos();

        int casa;
        Clases.csHerramientas llena = new Clases.csHerramientas();
        string foto;

        protected void Page_Load(object sender, EventArgs e)
        {
            llena.LlenarGridquery(gvProductos, "select * from productos");
        }

     

        protected void Imagebutton3_Click(object sender, ImageClickEventArgs e)
        {
            //Leer las variables

            casa = Convert.ToInt32(txtidprod.Text);


            Clases.CsProductos prod = new Clases.CsProductos();
            try
            {



                prod.Eliminar(this.casa);

                Response.Write("Registro Eliminado!");

                txtdescri.Text = "";
                txtprecio.Text = "";
                txtexis.Text = "";
                txtidprov.Text = "";
               
                foto = "";

                Response.Redirect("Productos.aspx");

            }

            catch (Exception ex)
            {

                Response.Write(ex.Message);

            }
        }

        protected void ImageModificar_Click(object sender, ImageClickEventArgs e)
        {
            casa = Convert.ToInt32(txtidprod.Text);

            Clases.CsProductos prod = new Clases.CsProductos();

            try
            {

                prod.Id_prod = this.casa;
                prod.Descripcion = txtdescri.Text;
                prod.Precio = txtprecio.Text;
               prod.Existencias = txtexis.Text;
               prod.Id_provf = txtidprov.Text;

                //Si selecciono un archivo


                prod.Modificar();


                Response.Write("Registro Modificado");


                Response.Redirect("Productos.aspx");

            }

            catch (Exception ex)
            {

                Response.Write(ex.Message);



            }
        }

        protected void ImageGuardar_Click(object sender, ImageClickEventArgs e)
        {
            if (fuFoto0.HasFile)
            {
                string fullPath = Path.Combine(Server.MapPath("Imagenes_png//"), llena.GenerarNombreArchivo(fuFoto0));
                fuFoto0.SaveAs(fullPath);
                foto = Path.GetFileName(fullPath);
            }

            llena.guardar_prod(txtdescri.Text, txtprecio.Text, txtexis.Text, foto, txtidprov.Text);
            Response.Redirect("Productos.aspx");
        }

        protected void gvProductos_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtidprod.Text = gvProductos.Rows[gvProductos.SelectedIndex].Cells[0].Text;
            txtdescri.Text = gvProductos.Rows[gvProductos.SelectedIndex].Cells[1].Text;
            txtprecio.Text = gvProductos.Rows[gvProductos.SelectedIndex].Cells[2].Text;
            txtexis.Text = gvProductos.Rows[gvProductos.SelectedIndex].Cells[3].Text;
            txtidprov.Text = gvProductos.Rows[gvProductos.SelectedIndex].Cells[6].Text;
        }

    }
}